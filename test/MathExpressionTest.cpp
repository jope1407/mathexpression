/**
 * @author  Daniel Wikström, Joakim Petersson
 * @file    MathExpressionTest.cpp
 * @section DESCRIPTION
 * Testfall för de olika medlemsfunktioner som MathExpression innehåller
 */
#include <catch.hpp>
#include <MathExpression.h>

TEST_CASE("Test utav calculate"){
    SECTION("Test av calculate: 1+1"){
        MathExpression expr("1+1");
        REQUIRE(expr.calculate() == 2.0);
    }
    SECTION("Test av calculate: 1+2+3"){
        MathExpression expr("1+2+3");
        REQUIRE(expr.calculate() == 6.0);
    }
    SECTION("Test av calculate: 1+10-5"){
        MathExpression expr("1+10-5");
        REQUIRE(expr.calculate() == 6.0);
    }
    SECTION("Test av calculate: 1+2-3+4+5-6+7-8+9+10"){
        MathExpression expr("1+2-3+4+5-6+7-8+9+10");
        REQUIRE(expr.calculate() == 21.0);
    }

    SECTION("Test av calculate: 2*2"){
        MathExpression expr("2*2");
        REQUIRE(expr.calculate() == 4.0);
    }
    SECTION("Test av calculate: 1+10*5"){
        MathExpression expr("1+10*5");
        REQUIRE(expr.calculate() == 51.0);
    }
    SECTION("Test av calculate: 2+3*5"){
        MathExpression expr("2+3*5");
        REQUIRE(expr.calculate() == 17.0);
    }

    SECTION("Test av calculate: 2*2*2"){
        MathExpression expr("2*2*2");
        REQUIRE(expr.calculate() == 8.0);
    }
    SECTION("Test av calculate: 3×(5+3×6)−5+2×(9÷2−3)"){
        MathExpression expr("3*(5+3*6)-5+2*(9/2-3)");
        REQUIRE(expr.calculate() == 67.0);
    }
}

TEST_CASE("Tester utav postfixNotation") {

    SECTION("Test postfix notation: 1+2") {
        MathExpression expr("1+2");
        REQUIRE(expr.postfixNotation() == "1 2 +");
    }
    SECTION("Test postfix notation: 12+2") {
        MathExpression expr("12+2");
        REQUIRE(expr.postfixNotation() == "12 2 +");
    }
    SECTION("Test postfix notation: 4*6+5") {
        MathExpression expr("4*6+5");
        REQUIRE(expr.postfixNotation() == "4 6 * 5 +");
    }

    SECTION("Test postfix notation: 4+6+5") {
        MathExpression expr("4+6+5");
        REQUIRE(expr.postfixNotation() == "4 6 + 5 +");
    }

    SECTION("Test postfix notation: 4+6*5") {
        MathExpression expr("4+6*5");
        REQUIRE(expr.postfixNotation() == "4 6 5 * +");
    }
    SECTION("Test postfix notation: 6+5-4") {
        MathExpression expr("6+5-4");
        REQUIRE(expr.postfixNotation() == "6 5 + 4 -");
    }
    SECTION("Test postfix notation: 6-5+4") {
        MathExpression expr("6-5+4");
        REQUIRE(expr.postfixNotation() == "6 5 - 4 +");
    }
    SECTION("Test postfix notation: 5*(27+3*7)+22") {
        MathExpression expr("5*(27+3*7)+22");
        REQUIRE(expr.postfixNotation() == "5 27 3 7 * + * 22 +");
    }
    SECTION("Test postfix notation: (4+6)*5") {
        MathExpression expr("(4+6)*5");
        REQUIRE(expr.postfixNotation() == "4 6 + 5 *");
    }
    SECTION("Test postfix notation: 1*2/7") {
        MathExpression expr("1*2/7");
        REQUIRE(expr.postfixNotation() == "1 2 * 7 /");
    }
    SECTION("Test postfix notation: 1*2/7*5/5") {
        MathExpression expr("1*2/7*5/5");
        REQUIRE(expr.postfixNotation() == "1 2 * 7 / 5 * 5 /");
    }

}
TEST_CASE("Test av isValid()"){
    SECTION("Test av fullständigt uttryck)"){
        MathExpression test("1+1*(4/2)-1");
        REQUIRE(test.isValid() == true);
    }
    SECTION("Test av inmatning av ord"){
        MathExpression test("Hello World!");
        REQUIRE(test.isValid() == false);
    }
    SECTION("Test av uttryck som avslutas med operator"){
        MathExpression test("43+12-");
        REQUIRE(test.isValid() == false);
    }
    SECTION("Test av uttryck utan operator mellan paranteser"){
        MathExpression test("(1+1)(1+1)");
        REQUIRE(test.isValid() == false);
    }
    SECTION("Test av uttryck som inte stänger parantes"){
        MathExpression test("(1+1)(1+1");
        REQUIRE(test.isValid() == false);
    }
    SECTION("Test av uttryck som har två operatorer i rad"){
        MathExpression test("1++1");
        REQUIRE(test.isValid() == false);
    }
    SECTION("Test av uttryck som innehåller whitespace"){
        MathExpression test("1 + 1 1");
        REQUIRE(test.isValid() == false);
    }
}

TEST_CASE("Test av = operatorn"){
    SECTION("Test av fungerande tilldelning"){
        MathExpression test("1 + 1");
        test = "1+2";
        REQUIRE(test.isValid() == true);
    }

    SECTION("Test av felaktig tilldelning"){
        MathExpression test("2+2");
        test = "1 + 1";
        REQUIRE(test.isValid() == true);
    }
}

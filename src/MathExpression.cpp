/**
 * @author  Daniel Wikström & Joakim Petersson
 * @file    MathExpression.cpp
 * @section DESCRIPTION
 * Implementation av medlemsfunktioner till klass för lagring och beräkning av enkla matematiska uttryck.
 *
*/

#include "MathExpression.h"
#include <vector>
#include <sstream>

/**
 * @brief Konstruktor
 * @param expression En sträng som innehåller uttrycket som skall beräknas
 */
MathExpression::MathExpression(const std::string &expression){
    testExpression = expression;
}

/**
 * @brief Testar om det inmatade uttrycket går att använda av de andra medlemsfunktionerna
 * @return True om uttrycket går igenom alla tester, annars false
 */

bool MathExpression::isValid() const{

    bool startParenthesis = false;
    bool endParenthesis = false;
    bool mathOperator = false; // Håller koll på om två operatorer i rad påträffas
    std::string operators = "+-*/";

    for(int i = 0; i < testExpression.length(); i++){
        char t = testExpression.at(i);

        //Om en andra startparantes påträffas innan en slutparantes returneras false.
        if(startParenthesis == true && t == '('){
            errorCode = 1;
            return false;
        } else if(t == '(' && endParenthesis == true){
            errorCode = 2;
            return false;
        } else if (t == '('){
            startParenthesis = true;
            mathOperator = false;

            continue;
        }

        //Om en andra operator påträffas innan en siffra så returneras false
        if(operators.find(t) != std::string::npos && mathOperator == true){
            errorCode = 3;
            return false;

        } else if(operators.find(t) != std::string::npos){
            mathOperator = true;
            endParenthesis = false;

            continue;
        }

        //Ändra parantes status om en slutparantes påträffas
        if(t == ')'){
            startParenthesis = false;
            endParenthesis = true;
            mathOperator = false;
            continue;
        }

        //Om en siffra påträffas, återställ operator status
        if(isdigit(t) && endParenthesis == false){
            mathOperator = false;
            continue;
        }

        //Om continue inte påträffats har felaktigt tecken påträffats, returnera false
        errorCode = 4;
        return false;

    }

    // Om en parantes inte stängs, returnera falskt
    if(startParenthesis == true){
        errorCode = 5;
        return false;
    }

    // Om sista tecknet är en operator, returnera falskt
    if(mathOperator == true){
        errorCode = 6;
        return false;
    }

    return true;
}


/**
 * @brief Ger det aktuella uttrycket på infix notation
 * @return En sträng innehållandes det ursprungliga uttrycket
 */
std::string MathExpression::infixNotation() const{
    return testExpression;
}


/**
 * @brief Ger det aktuella uttrycket i postfix notation
 * @return En sträng med uttrycket i postfix notation
 */
std::string MathExpression::postfixNotation()  const {

    std::string output; //sträng som håller uttrycket i postfix
    std::vector<char> op_stack; // En stack där operatorer placeras

    for(int i = 0; i< testExpression.length(); i++){ // Går igenom hela strängen med uttrycket

        // Kollar om det är + eller -
        if(testExpression.at(i) == '+' || testExpression.at(i) == '-'){
            if(!op_stack.empty() && op_stack.back()!= '('){ // Om stacken inte är tom kommer det pga prioriteringsreglerna inte finnas en operator med lägre prio...

               // Popar därför ut den som ligger överst till output.
               output += " ";
               output += op_stack.back();
               op_stack.pop_back();
            }
            output += " ";
            op_stack.push_back(testExpression.at(i));
        }

        // Kollar om det är * eller /
        else if (testExpression.at(i) == '/' || testExpression.at(i) == '*'){

            // Kollar om det finns operator av samma prio på stacken och poppar isf
            if((!op_stack.empty() && op_stack.back() == '*' ) ||(!op_stack.empty() && op_stack.back() == '/')){
                output += " ";
                output += op_stack.back();
                output += " ";
                op_stack.pop_back();
                op_stack.push_back(testExpression.at(i));
            }
            else{ // Annars pusha in operatorn på stacken
                output += " ";
                op_stack.push_back(testExpression.at(i));
            }

        }

        // Kollar om vänster-parantes
        else if (testExpression.at(i) == '('){
            op_stack.push_back('(');
        }

        // Kollar om Höger-parantes
        else if (testExpression.at(i) == ')'){
            bool found = false; // När motsvarande vänsterparantes hittats så blir denna true

            while(!found && !op_stack.empty()){ // Går igenom tills vänsterparantes hittats eller stacken är tom
                if(op_stack.back() == '('){
                    op_stack.pop_back();
                    found = true;
                    break;
                }
                else{ // Poppar ut allt fram till vänsterparantesen till output
                    output += " ";
                    output += op_stack.back();
                    op_stack.pop_back();
                }
            }
        }

        else{ // En vanlig siffra påträffades om vi når hit och den läggs till output
            output+= testExpression.at(i);
        }
    }

    // När hela uttrycket gåtts igenom så poppas det som ligger på stacken till output
    while(!op_stack.empty()){
        output += " ";
        output += op_stack.back();
        op_stack.pop_back();
    }

    return output;
}


/**
 * @brief Beräknar det aktuella uttrycket
 * @return Ger resultatet av beräkningen som en double
 */
double MathExpression::calculate() const{
    std::string postfix = this->postfixNotation(); // Uttrycket i postfix-notation
    std::istringstream ist(postfix);

    std::string tmpStr;
    std::vector<double> operands; // Vector som håller operanderna

    while(ist >> tmpStr){ // Läser ur varje del av postfix-uttrycket till en temporär sträng

        if(tmpStr == "+"){ // Om det är ett + som lästs

            // poppar de bakersta två operanderna, adderar och pushar resultatet på stacken
            double a = operands.back();
            operands.pop_back();
            double b = a + operands.back();
            operands.pop_back();
            operands.push_back(b);
        }

        else if(tmpStr == "-") // Om det är ett - som lästs
        {
            double a = operands.back();
            operands.pop_back();
            double b = operands.back()-a;
            operands.back();
            operands.pop_back();

            operands.push_back(b);
        }
        else if(tmpStr == "*"){ // Om det är * som lästs

            double a = operands.back();
            operands.pop_back();
            double b = operands.back();
            operands.pop_back();

            operands.push_back(b*a);
        }

        else if(tmpStr == "/"){ // Om det är / som lästs
            double a = operands.back();
            operands.pop_back();
            double b = operands.back();
            operands.pop_back();

            operands.push_back(b/a);
        }
        else{ // Når vi hit så var det ett nummer som lästes och ingen operator
            std::istringstream is_tmp(tmpStr);
            double d;
            is_tmp >> d;
            operands.push_back(d);
        }
    }

    // Här skall nu endast ett värde finnas och det ska vara resultatet av uttrycket
    return operands.front();

}

/**
 * @brief Avgör vilket felmeddelande som ska visas baserat på medlemsvariabeln errorCode
 * @return Ett felmeddelande lagrat i en sträng baserat på innehållet i medlemsvariabeln errorCode
 */
std::string MathExpression::errorMessage() const{

    switch(errorCode){
        case 1:
            return "Only one level of parenthesis can be used in this calculator!";
        case 2:
            return "You need to have a operator between parenthesis blocks!";
        case 3:
            return "You cannot have two operators in a row!";
        case 4:
            return "You can only use +-*/ and numbers. No whitespace or letters!";
        case 5:
            return "You have to close all paranthesis blocks!";
        case 6:
            return "The expression cannot end with a operator!";
        case 0:
            return "";

    }
}

/**
  * @brief Överlagrad tilldelningoperator för strängar
  * @param expression Ett matematiskt uttryck i strängformat med max en parantesnivå
  * @return Ett MathExpression object som innehåller det nya värdet om uttrycket klarar av isValid() testet, annars returneras ett objekt med det gamla värdet.
  */
 MathExpression& MathExpression::operator=(const std::string& expression){

    //Sparar temporärt det nya uttrycket i ett MathExpression object för att kunna använda isValid()
    MathExpression temp(expression);

    //Kollar om det nya uttrycket är giltigt, i så fall sparas det nya uttrycket
    if(temp.isValid()){
        testExpression = expression;
        errorCode = 0;
    }

    return *this;
}
